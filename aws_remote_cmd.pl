#!/usr/bin/perl

use Modern::Perl;
use Net::SSH::Perl;

$\ = "\n";

my $cert = $ARGV[0];
my $user = $ARGV[1];
my $host = $ARGV[2];
my $cmd  = $ARGV[3]


my $ssh = Net::SSH::Perl->new( $host, identity_files => [ $cert ] );

$ssh->login( $user );

my ( $stdout, $stderr ) = $ssh->cmd($cmd);

print "STDOUT: $stdout" if $stdout;
print "STDERR: $stderr" if $stderr;
